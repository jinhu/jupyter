FROM registry.gitlab.com/jinhu/python:3.6.1


RUN apk add --no-cache alpine-sdk gcc musl-dev \
&&  pip install --upgrade pip \
&&  pip install jupyter \    
&&  pip install jupyter-c-kernel \
&&  pip install RISE \
&&  jupyter-nbextension install rise --py --sys-prefix \
&&  jupyter-nbextension enable rise --py --sys-prefix

WORKDIR /srv/notebook

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0"]